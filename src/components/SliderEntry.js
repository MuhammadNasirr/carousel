import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import PropTypes from 'prop-types';
import styles from "example/src/styles/SliderEntry.style";
import { Icon, Button } from "native-base";

export default class SliderEntry extends Component {

    static propTypes = {
        data: PropTypes.object.isRequired,
        even: PropTypes.bool,
        parallax: PropTypes.bool,
        parallaxProps: PropTypes.object
    };



    render() {
        const { data: { title, buttonName, subtitle, price } } = this.props;
        const uppercaseTitle = title ? (
            <Text
                style={[styles.title]}
                numberOfLines={2}
            >
                {title}
            </Text>
        ) : false;

        return (
            <TouchableOpacity
                activeOpacity={1}
                style={styles.slideInnerContainer}
            >
                <View style={{ backgroundColor: '#fff', borderRadius: 8 }} >
                    <View style={[]}>
                        <View style={{ flexDirection: 'row', padding: 10 }}>
                            <Text style={{ color: '#ff6b45' }}>starting at </Text>
                            <View style={{ width: 50, height: 20, borderRadius: 10, padding: 0, alignItems: "center", backgroundColor: '#ff6b45' }}><Text style={{ color: '#fff' }}>{price}</Text></View>
                            {/* <View style={[styles.radiusMask]} /> */}
                        </View>
                    </View>
                    <View style={[styles.textContainer, {}]}>
                        <View>
                            {uppercaseTitle}
                        </View>
                        <Text
                            style={[styles.subtitle]}
                            numberOfLines={8}
                        >
                            {subtitle}
                        </Text>
                    </View>
                    <View style={{ flexDirection: 'row', padding: 10 }}>
                        <View style={{ width: 20, height: 20, borderRadius: 20, padding: 0, alignItems: "center", backgroundColor: '#ff6b45' }}><Text style={{ color: '#fff' }}>i</Text></View>
                        <Text style={{ color: '#ff6b45', marginLeft: 3, textDecorationLine: 'underline' }}>How it is works</Text>
                    </View>

                    <Button style={{ width: '85%', margin: 15, backgroundColor: '#ff6b45', justifyContent: 'center' }}>
                        <Text style={{ color: '#fff', fontSize: 16 }}>{buttonName}</Text>
                    </Button>

                </View>
            </TouchableOpacity>
        );
    }
}
