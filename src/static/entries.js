export const ENTRIES1 = [
    {
        title: 'Stretch Theraphy',
        subtitle: 'Start your day with a personalized stretch in the confort of your home or office. Improve your mood in just 45min! ',
        illustration: 'https://i.imgur.com/UYiroysl.jpg',
        buttonName:'Book Stretch',
        price:'$89'
    },
    {
        title: 'Massage',
        subtitle: 'Restore and detch from reality. We combine a veriety of massage techniques, for a fully customized experience.',
        illustration: 'https://i.imgur.com/UPrs1EWl.jpg',
        buttonName:'Book Massage',
        price:'$99'
    },
    {
        title: 'IV Hydration',
        subtitle: 'Our nurses provide a proprietary blend of IV infusions meant to cure anything from hangovers to flu symptoms, weight-loss and more!',
        illustration: 'https://i.imgur.com/MABUbpDl.jpg',
        buttonName:'Book Hydration',
        price:'$199'
    },
    
];

export const ENTRIES2 = [
    {
        title: 'Favourites landscapes 1',
        subtitle: 'Lorem ipsum dolor sit amet',
        illustration: 'https://i.imgur.com/SsJmZ9jl.jpg'
    },
    {
        title: 'Favourites landscapes 2',
        subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
        illustration: 'https://i.imgur.com/5tj6S7Ol.jpg'
    },
    {
        title: 'Favourites landscapes 3',
        subtitle: 'Lorem ipsum dolor sit amet et nuncat',
        illustration: 'https://i.imgur.com/pmSqIFZl.jpg'
    },
    {
        title: 'Favourites landscapes 4',
        subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
        illustration: 'https://i.imgur.com/cA8zoGel.jpg'
    },
    {
        title: 'Favourites landscapes 5',
        subtitle: 'Lorem ipsum dolor sit amet',
        illustration: 'https://i.imgur.com/pewusMzl.jpg'
    },
    {
        title: 'Favourites landscapes 6',
        subtitle: 'Lorem ipsum dolor sit amet et nuncat',
        illustration: 'https://i.imgur.com/l49aYS3l.jpg'
    }
];
