import React, { Component } from "react";
import { Platform, View, ImageBackground, ScrollView, Text, StatusBar } from "react-native";
import Carousel, { Pagination } from "react-native-snap-carousel";
import { sliderWidth, itemWidth } from "example/src/styles/SliderEntry.style";
import SliderEntry from "example/src/components/SliderEntry";
import styles, { colors } from "example/src/styles/index.style";
import { ENTRIES1, ENTRIES2 } from "example/src/static/entries";


// import BackgroundeOne from '../assets/video/ivtherapyvideo-bg.mp4'
// import BackgroundeTwo from '../assets/video/stretchviodeo-bg.mp4'

import BackgroundeThree from "../assets/images/stretchimage-bg.jpg";
import BackgroundeOne from "../assets/images/couplesmassageimage-bg.jpg";
import BackgroundeTwo from "../assets/images/ivtherapyimage-bg.jpg";
import { Button, Icon, Fab } from "native-base";

const IS_ANDROID = Platform.OS === 'android';
const SLIDER_1_FIRST_ITEM = 1;

const backgrounds = [BackgroundeOne, BackgroundeTwo, BackgroundeThree]

export default class example extends Component {

    constructor(props) {
        super(props);
        this.state = {
            slider1ActiveSlide: SLIDER_1_FIRST_ITEM,
            background: BackgroundeOne,
            active: 'true'
        };
    }

    _renderItem({ item, index }) {
        return <SliderEntry data={item} even={(index + 1) % 2 === 0} />;
    }

    changeBackground = (index) => {
        let currentBg = this.state.background
        let newIndex = backgrounds.indexOf(currentBg) + 1
        let newBg = (newIndex) == backgrounds.length ? backgrounds[0] : backgrounds[newIndex]
        console.log('aasa')
        this.setState({ slider1ActiveSlide: index })
        this.setState({
            background: newBg
        })
    }

    _renderItemWithParallax({ item, index }, parallaxProps) {
        return (
            <SliderEntry
                data={item}
                even={(index + 1) % 2 === 0}
                parallax={true}
                parallaxProps={parallaxProps}
            />
        );
    }



    mainExample(number, title) {
        const { slider1ActiveSlide } = this.state;

        return (
            <View style={{}}>

                <Carousel
                    ref={c => this._slider1Ref = c}
                    data={ENTRIES1}
                    renderItem={this._renderItemWithParallax}
                    sliderWidth={sliderWidth}
                    itemWidth={itemWidth}
                    hasParallaxImages={true}
                    firstItem={SLIDER_1_FIRST_ITEM}
                    inactiveSlideScale={0.94}
                    inactiveSlideOpacity={0.6}
                    inactiveSlideShift={20}
                    containerCustomStyle={styles.slider}
                    contentContainerCustomStyle={styles.sliderContentContainer}
                    loop={true}
                    loopClonesPerSide={2}
                    autoplay={true}
                    autoplayDelay={500}
                    autoplayInterval={3000}
                    onSnapToItem={(index) => { this.changeBackground(index) }

                    }
                />
                <Pagination
                    dotsLength={ENTRIES1.length}
                    activeDotIndex={slider1ActiveSlide}
                    containerStyle={styles.paginationContainer}
                    dotColor={'rgba(255, 255, 255, 0.92)'}
                    dotStyle={styles.paginationDot}
                    inactiveDotColor={colors.black}
                    inactiveDotOpacity={0.4}
                    inactiveDotScale={0.6}
                    carouselRef={this._slider1Ref}
                    tappableDots={!!this._slider1Ref}
                />
            </View>
        );
    }


    render() {
        const example1 = this.mainExample(1, 'Default layout | Loop | Autoplay | Parallax | Scale | Opacity | Pagination with tappable dots');
        const { slider1ActiveSlide } = this.state;
        // alert(slider1ActiveSlide)
        // const { data: { title } } = this.props;
        return (
            // alert(title),

            // <SafeAreaView style={styles.safeArea}>
            <View style={{ justifyContent: 'space-between', alignItems: 'center', flex: 2 }}>
                <StatusBar
                    translucent={true}
                    backgroundColor={'rgba(0, 0, 0, 0.3)'}
                    barStyle={'light-content'}
                />
                {/* { this.gradient } */}
                <ImageBackground style={styles.MainContainer}
                    resizeMode='cover'
                    source={this.state.background}
                >
                    <ScrollView
                        style={styles.scrollview}
                        scrollEventThrottle={200}
                        directionalLockEnabled={true}
                    >

                        <View style={{ justifyContent: 'space-between', marginTop: 30, flex: 1 }}>
                            {
                                slider1ActiveSlide < 2 ?
                                    <View style={{ marginTop: 60 }}>
                                        <View style={{ borderWidth: 2, borderRadius: 5, width: 150, marginLeft: '10%', alignItems: 'center', borderColor: '#fff', padding: 5 }}>
                                            <Text style={{ fontSize: 12, color: '#fff' }}>{`REME concierge here`}</Text>
                                        </View>
                                        <View style={{ borderWidth: 2, borderRadius: 5, width: 100, marginTop: 10, marginLeft: '10%', alignItems: 'center', borderColor: '#fff', padding: 5 }}>
                                            <Text style={{ fontSize: 12, color: '#fff', marginLeft: '10%' }}>{`Do you need any help?`}</Text>
                                        </View>
                                    </View>
                                    :
                                    <View style={{ marginTop: 30 }}>
                                        <View style={{ borderWidth: 2, opacity: 0.3, borderRadius: 5, width: 150, marginLeft: '10%', alignItems: 'center', borderColor: '#fff', padding: 5 }}>
                                            <Text style={{ fontSize: 12, color: '#fff' }}>{`REME concierge here`}</Text>
                                        </View>
                                        <View style={{ borderWidth: 2, borderRadius: 5, width: 100, marginTop: 10, marginLeft: '10%', alignItems: 'center', borderColor: '#fff', padding: 5 }}>
                                            <Text style={{ fontSize: 12, color: '#fff', marginLeft: '10%' }}>{`Do you need any help?`}</Text>
                                        </View>

                                        <View style={{ borderWidth: 2, borderRadius: 5, width: 150, marginTop: 10, marginLeft: '10%', alignItems: 'center', borderColor: '#fff', backgroundColor: '#ff6b45', padding: 5 }}>
                                            <Text style={{ fontSize: 12, color: '#fff' }}>{`Tap here to continue`}</Text>
                                        </View>
                                    </View>
                            }
                            <View>
                                <Text style={{ fontSize: 32, color: '#fff', marginLeft: '10%' }}>{`Hi Raphael!`}</Text>
                                <Text style={{ fontSize: 16, color: '#d3d3d3', marginLeft: '10%' }}>{`SELECT A SERVICE`}</Text>
                            </View>
                            <View style={{}}>
                                {example1}
                            </View>
                            <View>
                                <Text style={{ fontSize: 14, marginTop: 40, color: '#d3d3d3', marginLeft: '10%' }}>{`WANT MORE?`}</Text>
                                <Text style={{ fontSize: 26, color: '#fff', marginLeft: '10%', marginBottom: 20 }}>{`Ecplore other services`}</Text>
                            </View>
                        </View>

                    </ScrollView>
                    <Fab
                        active={this.state.active}
                        direction='up'
                        containerStyle={{}}
                        style={{ backgroundColor: '#fff', width: 35, height: 35 }}
                        position='topRight'
                        onPress={() => this.setState({ active: !this.state.active })}>
                        <Icon name='align-left' type='FontAwesome' style={{ fontSize: 18, color: '#000' }} />
                    </Fab>
                    <Fab
                        active={this.state.active}
                        direction='up'
                        containerStyle={{}}

                        style={{ backgroundColor: '#fff', width: 35, height: 35 }}
                        position='bottomRight'
                        onPress={() => this.setState({ active: !this.state.active })}>
                        <Icon name='arrow-up' style={{ color: '#000' }} />
                    </Fab>
                    <Fab
                        active={this.state.active}
                        direction='up'
                        containerStyle={{}}
                        style={{ backgroundColor: '#fff', width: 35, height: 35 }}
                        position='topLeft'
                        onPress={() => this.setState({ active: !this.state.active })}>
                        <Icon name='send-o' type='FontAwesome' style={{ fontSize: 18, color: '#000' }} />
                    </Fab>
                </ImageBackground>
            </View>
            // </SafeAreaView>
        );
    }
}


// "react-native-snap-carousel": "file:.."
